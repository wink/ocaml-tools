open Core.Std
open ExtLib
open Omd

(*
let rec foo l =
    match l with
    | [] -> "[]"
    | hd :: tl ->
        (match hd with
        | Url (_, _, _) -> (to_html [hd]) ^ foo tl
        | x -> "N" ^ (Omd_backend.sexpr_of_md [x]) ^ foo tl)
;;
    | Url (href, t, title) as hd :: tl -> (to_html [hd]) ^ foo tl
*)

let rec bar l =
    match l with
    | hd :: tl -> foo hd ^ bar tl
    | [] -> ""
and

 foo l =
    match l with
    | Url (href, t, _) :: tl        -> href ^ "\n" ^ foo t ^ foo tl
    | Paragraph (t) :: tl           -> "" ^ foo t ^ foo tl
    | Code("", _) :: tl             -> "" ^ foo tl
    | Code_block("", _) :: tl       -> "" ^ foo tl
    | H1 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | H2 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | H3 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | H4 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | H5 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | H6 (t) :: tl                  -> "" ^ foo t ^ foo tl
    | Emph (t) :: tl                -> "" ^ foo t ^ foo tl
    | Bold (t) :: tl                -> "" ^ foo t ^ foo tl
    | Ul (t) :: tl                  -> "" ^ bar t ^ foo tl
    | Ol (t) :: tl                  -> "" ^ bar t ^ foo tl
    | Ulp (t) :: tl                 -> "" ^ bar t ^ foo tl
    | Olp (t) :: tl                 -> "" ^ bar t ^ foo tl
    | Blockquote (t) :: tl          -> "" ^ foo t ^ foo tl
    | Ref (r, n, a, f) :: tl        -> "[" ^ n ^ "." ^ n ^ "." ^ a ^ "." ^ a ^ "]" ^ foo tl
    | (Text _|Code _|Code_block _|Br|Hr|NL|Ref _|Img_ref _|Raw _|Raw_block _
      |Html _|Html_block _|Html_comment _|Img _|X _) :: tl -> "" ^ foo tl
    | [] -> ""
;;

let do_cat file =
    In_channel.with_file file ~f:(fun ic ->
        let text = input_all ic in
        let md = of_string text in
            print_endline text;
            print_endline "-----";
            print_endline (to_html md);
            print_endline "-----";
            print_endline (to_markdown md);
            print_endline "-----";
            print_endline (Omd_backend.sexpr_of_md md);
            print_endline "-----";
            print_string (foo md);
    )

let spec =
    let open Command.Spec in
    empty
    +> anon ("filename" %: string)

let command =
    Command.basic
        ~summary:"cat"
        ~readme:(fun () -> "details")
        spec
        (fun filename () -> do_cat filename)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
