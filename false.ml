open Core.Std

let do_false =
    let _ =
    exit 1
    in ()

let spec =
    let open Command.Spec in
    empty

let command =
    Command.basic
        ~summary:"false"
        ~readme:(fun () -> "details")
        spec
        (fun () -> do_false)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
