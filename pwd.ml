open Core.Std

let do_pwd =
    let _ =
    let d = Sys.getcwd() in
    print_bytes (d); print_endline ""
    in ()

let spec =
    let open Command.Spec in
    empty

let command =
    Command.basic
        ~summary:"pwd"
        ~readme:(fun () -> "details")
        spec
        (fun () -> do_pwd)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
