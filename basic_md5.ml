open Core.Std

let do_hash file =
    In_channel.with_file file ~f:(fun ic ->
        let open Cryptokit in
        hash_channel (Hash.md5 ()) ic
        |> transform_string (Hexa.encode ())
        |> print_endline
    )

let spec =
    let open Command.Spec in
    empty
    +> anon ("filename" %: string)

let command =
    Command.basic
        ~summary:"Generate MD5"
        ~readme:(fun () -> "More detailed information")
        spec
        (fun filename () -> do_hash filename)

let () =
    Command.run ~version:"1.0" ~build_info:"RWO" command
