open Core.Std

let do_whoami =
    let _ =
    let u = Unix.getlogin() in
    print_bytes (u); print_endline ""
    in ()

let spec =
    let open Command.Spec in
    empty

let command =
    Command.basic
        ~summary:"whoami"
        ~readme:(fun () -> "details")
        spec
        (fun () -> do_whoami)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
