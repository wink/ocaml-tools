open Core.Std

let do_true =
    let _ =
    exit 0
    in ()

let spec =
    let open Command.Spec in
    empty

let command =
    Command.basic
        ~summary:"true"
        ~readme:(fun () -> "details")
        spec
        (fun () -> do_true)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
