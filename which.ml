open Core.Std
open Core.Core_string

let f x =
    let r = Sys.file_exists x in
    match r with
    | `Yes -> print_endline x
    | `No | `Unknown -> print_string ""

let do_which name =
    begin
    try
        let _ =
        let path = Sys.getenv "PATH" in
        let dirs = match path with
        | Some p ->  split p ~on:':'
        | None -> [] in
        let paths = List.map ~f:(fun x -> x ^ Filename.dir_sep ^ name) dirs in
(*
        List.map ~f:print_endline paths in 
        List.filter ~f:Sys.file_exists paths in ()
*)
        List.map ~f:f paths in ()
    with Not_found ->
      print_newline ();
    end;;

let spec =
    let open Command.Spec in
    empty
    +> anon ("name" %: string)

let command =
    Command.basic
        ~summary:"which"
        ~readme:(fun () -> "details")
        spec
        (fun name () -> do_which name)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
