open Core.Std

let do_cat file =
    In_channel.with_file file ~f:(fun ic ->
    try
        while true; do
        let line = input_line ic in
            print_endline line;
        done;
    with End_of_file ->
       print_string "";
    )

let spec =
    let open Command.Spec in
    empty
    +> anon ("filename" %: string)

let command =
    Command.basic
        ~summary:"cat"
        ~readme:(fun () -> "details")
        spec
        (fun filename () -> do_cat filename)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
