open Core.Std

let do_tac file =
    let _ = let lines = In_channel.read_lines file in
    List.map ~f:print_endline (List.rev lines) in ()

let spec =
    let open Command.Spec in
    empty
    +> anon ("filename" %: string)

let command =
    Command.basic
        ~summary:"tac"
        ~readme:(fun () -> "details")
        spec
        (fun filename () -> do_tac filename)

let () =
    Command.run ~version:"1.0" ~build_info:"foo" command
